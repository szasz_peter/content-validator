package com.epam.idcollector;

import com.epam.idcollector.Extractor.Extractor;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;

import static com.epam.idcollector.Helper.parseXML;
import static com.epam.idcollector.Main.imageMap;
import static com.epam.idcollector.Main.atlasTopicSetPath;
import static com.epam.idcollector.Main.target;

/**
 * Created by Student on 7/31/2017.
 */
public class Validator {
    private static Logger logger = Logger.getLogger(Validator.class);
    private static Extractor extractor = new Extractor();
    private static Helper helper = new Helper();
    private static final String FILE_SEPARATOR = "\\";
    private static final String QUALIFICATION_VALID = " qualification='VALID' ";
    private static final String QUALIFICATION_INVALID = " qualification='INVALID' ";
    private static final String CONTRIBUTOR = "validation='CONTRIBUTOR_VALIDATION' ";
    private static final String OVERVIEW = "validation='OVERVIEW_VALIDATION' ";
    private static final String METADATA = "validation='ATS_NODE_METADATA_VALIDATION' ";
    private static final String RECOMMENDED_TOPICS = "validation='RECOMMENDED_TOPIC_VALIDATION' ";
    private static final String TREATMENT_BY_STATE = "validation='TREATMENT_BY_STATE_VALIDATION' ";
    private static final String KEY_PRIMARY_SOURCE = "validation='KEY_PRIMARY_SOURCE_VALIDATION' ";
    private static final String FORMS_AND_INSTRUCTIONS = "validation='FORMS_AND_INSTRUCTIONS_VALIDATION' ";

    public void validateContributors(Document xml, String docId) {

        List<String> names = extractor.getContributorName(xml, docId);
        List<String> affiliations = extractor.getContributorAffiliation(xml, docId);
        List<String> biographies = extractor.getContributorBiography(xml, docId);
        List<String> images = extractor.getContributorImages(xml, docId);

        if (!names.isEmpty()) {
            logger.info(QUALIFICATION_VALID + CONTRIBUTOR + "comment='document contains author(s).'" + target);

            if (!(names.size() <= (affiliations.size() + images.size()))) {
                if ((affiliations.size() + images.size()) < names.size()) {
                    logger.error(QUALIFICATION_INVALID + CONTRIBUTOR + "comment='not every author has affiliation or graphic tag in xml structure, if affiliation is not present.'" + target);
                }
            }

            if (names.size() != biographies.size()) {
                if (biographies.size() < names.size()) {
                    logger.error(QUALIFICATION_INVALID + CONTRIBUTOR + "comment='not every author has biography tag in xml structure.'" + target);
                } else
                    logger.error(QUALIFICATION_INVALID + CONTRIBUTOR + "comment='some authors have more than one biographies.'" + target);
            }

            if (affiliations.isEmpty() && images.isEmpty())
                logger.error(QUALIFICATION_INVALID + CONTRIBUTOR + "comment='missing both affiliation and image.'" + target);

            names.forEach(name -> {
                if (name.startsWith("Enter")) {
                    logger.error(QUALIFICATION_INVALID + CONTRIBUTOR + "comment='Missing author name.'" + target);
                } else
                    logger.info(QUALIFICATION_VALID + CONTRIBUTOR + "comment='author name is filled properly.'" + target + " content=' " + name + "'");
            });

            affiliations.forEach(affiliation -> {
                if (affiliation.startsWith("Enter")) {
                    logger.error(QUALIFICATION_INVALID + CONTRIBUTOR + "comment='missing affiliation info.'" + target);
                } else
                    logger.info(QUALIFICATION_VALID + CONTRIBUTOR + "comment='author affiliation is filled properly.'" + target);
            });

            biographies.forEach(biography -> {
                if (biography.startsWith("Enter")) {
                    logger.error(QUALIFICATION_INVALID + CONTRIBUTOR + "comment='missing biography data.'" + target);
                } else
                    logger.info(QUALIFICATION_VALID + CONTRIBUTOR + "comment='author biography is filled properly.'" + target);
            });

            if (!images.isEmpty()) {
                images.forEach(image -> {
                    logger.info(QUALIFICATION_VALID + CONTRIBUTOR + "comment='author has an image associated." + target + " content=' " + image + "'");

                    if (!helper.doesContain(imageMap.get(xml.getDocumentURI().substring(xml.getDocumentURI().lastIndexOf("IPACK-WKUS-TAL"), xml.getDocumentURI().lastIndexOf("/Documents/"))), image))
                        logger.error(QUALIFICATION_INVALID + CONTRIBUTOR + "comment='image can not be located.'" + target + " content=' " + image + "'");
                    else
                        logger.info(QUALIFICATION_VALID + CONTRIBUTOR + "comment='image is properly located.'" + target + " content=' " + image + "'");
                });
            }
        } else
            logger.error(QUALIFICATION_INVALID + CONTRIBUTOR + "comment='document does not contain any authors.'" + target);
    }

    public void validateOverview(Document xml, String docId) {
        String regex = "((?:ftp|http|https)(?::\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s\"]*))";

        List<String> videoLinks = extractor.getVideoLinks(xml, docId);
        List<String> inlineInfographicImages = extractor.getInlineInfographicImages(xml, docId);
        List<String> interactiveInfoGraphics = extractor.getInteractiveInfoGraphics(xml, docId);
        List<String> nestedWkdocs = extractor.getNestedWkdocs(xml, docId);

        videoLinks.forEach(link -> {
            try {
                if (link.matches(regex) || link.equals("www.pay.gov"))
                    logger.info(QUALIFICATION_VALID + OVERVIEW + "comment='Overview video link is displayed and valid.'" + target + " content=' " + link + "'");
                else
                    logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='Overview video link is invalid.'" + target + " content=' " + link + "'");
            } catch (RuntimeException e) {
                logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='Runtime Exception!'" + target);
            }
        });

        inlineInfographicImages.forEach(image -> {

            if (!helper.doesContain(imageMap.get(xml.getDocumentURI().substring(xml.getDocumentURI().lastIndexOf("IPACK-WKUS-TAL"), xml.getDocumentURI().lastIndexOf("/Documents/"))), image))
                logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='inline infographic image can not be located.'" + target + " content=' " + image + "'");
            else
                logger.info(QUALIFICATION_VALID + OVERVIEW + "comment='inline infographic image is displayed and valid.'" + target + " content=' " + image + "'");
        });

        interactiveInfoGraphics.forEach(info -> {
            try {
                if (info.matches(regex))
                    logger.info(QUALIFICATION_VALID + OVERVIEW + "comment='Interactive infographic url is valid.'" + target + " content=' " + info + "'");
                else
                    logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='Interactive infographic url is invalid.'" + target + " content=' " + info + "'");
            } catch (RuntimeException e) {
                logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='Runtime Exception!'" + target);
            }
        });

        if (!nestedWkdocs.isEmpty()) {
            logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='document contains illegal nested documents.'" + target);
            nestedWkdocs.forEach(nDoc -> {
                logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='document contains nested document inside the analytical-commentary section.'" + target + " content= '" + nDoc + "'");
            });
        } else
            logger.info(QUALIFICATION_VALID + OVERVIEW + "comment='document does not contain illegal nested documents.'" + target);

        if (xml.getDocumentURI().substring(xml.getDocumentURI().lastIndexOf("/") + FILE_SEPARATOR.length()).equals("tpj01_WK-US_DOCUMENTS.xml")) {
            List<String> linkTools = extractor.getLinksTools(xml, docId);
            List<String> linkTitles = extractor.getLinksToolsTitles(xml, docId);
            List<String> linkVisibleTexts = extractor.getLinksToolsVisibleText(xml, docId);
            List<String> linkNodeRefs = extractor.getLinksToolsNodeRef(xml, docId);

            if (linkTools.size() <= linkTitles.size() && linkTools.size() <= linkVisibleTexts.size() && linkTools.size() <= linkNodeRefs.size())
                logger.info(QUALIFICATION_VALID + OVERVIEW + "comment='link tools: titles, visible texts and node-refs are all present.'" + target);
            else {
                if (linkTitles.size() < linkTools.size())
                    logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='link tools: titles metadata is not present everywhere.'" + target);

                if (linkVisibleTexts.size() < linkTools.size())
                    logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='link tools: visible texts metadata is not present everywhere.'" + target);

                if (linkNodeRefs.size() < linkTools.size())
                    logger.error(QUALIFICATION_INVALID + OVERVIEW + "comment='link tools: ats-node-ref metadata is not present everywhere.'" + target);
            }
        }
    }

    public void validateMetaDataAtsNodes(List<String> metaDataATSNodeIds, List<String> atlasTopicNodeIds) {
        metaDataATSNodeIds.forEach(node -> {
            if (helper.doesContain(atlasTopicNodeIds, node))
                logger.info(QUALIFICATION_VALID + METADATA + "comment='ATS metadata node from xml document is properly located in ATS xml.'" + target + " content=' " + node + "'");
            else
                logger.error(QUALIFICATION_INVALID + METADATA + "comment='ATS metadata node from xml document is missing from ATS xml.'" + target + " content=' " + node + "'");
        });
    }

    public void validateAts(List<String> allMetaDataATSNodeIds, List<String> atlasTopicNodeIds) {
        atlasTopicNodeIds.forEach(aNode -> {
            if (!helper.doesContain(allMetaDataATSNodeIds, aNode))
                logger.warn(QUALIFICATION_INVALID + METADATA + "comment='Atlas Topic Nodes xml contains ATS node that is not located in any of the xml documents.'" + target + " content=' " + aNode + "'");
        });
    }

    public void validateStateWidgets(Document xml, String docId) {

        List<String> stateWidgets = extractor.getStateWidgets(xml, docId);

        if (!stateWidgets.isEmpty()) {
            List<String> taxTypes = extractor.getStateWidgetTaxTypes(xml, docId);

            stateWidgets.forEach(widget -> {
                if (helper.doesContain(taxTypes, widget))
                    logger.info(QUALIFICATION_VALID + TREATMENT_BY_STATE + "comment='Treatment by State widget tax type is properly displayed in xml." + target + " content=' " + widget + "'");
                else
                    logger.error(QUALIFICATION_VALID + TREATMENT_BY_STATE + "comment='Treatment by State widget tax type is missing from xml.'" + target + " content=' " + widget + "'");
            });
        } else
            logger.error(QUALIFICATION_VALID + TREATMENT_BY_STATE + "comment='document is missing the Treatment by State widget.'" + target);

    }

    public void validateRecommendedTopics(Document xml, String docId) {

        Document ats = null;
        try {
            ats = parseXML(atlasTopicSetPath);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }


        if (!extractor.getRecommendedTopicsExploreTopics(xml, docId).isEmpty()) {
            List<String> exploreTopicContent = extractor.getContentOfExploreTopics(xml, docId);

            if (exploreTopicContent.isEmpty())
                logger.info(QUALIFICATION_VALID + RECOMMENDED_TOPICS + "comment='the document contains the empty Explore This Topic section in the Recommended Topics Widget.'" + target);
            else
                logger.error(QUALIFICATION_INVALID + RECOMMENDED_TOPICS + "comment='the document contains the Explore This Topic section, but it is not empty!'" + target + " content=' " + exploreTopicContent + "'");
        } else
            logger.error(QUALIFICATION_INVALID + RECOMMENDED_TOPICS + "comment='document is missing the Explore This Topic section from the Recommended Topics Widget!'" + target);

        List<String> exploreThisTopicContent = extractor.getExploreThisTopicFromAts(ats, extractor.getDocTitles(xml, docId).get(0));
        exploreThisTopicContent.addAll(extractor.getExploreThisTopicParent(ats, extractor.getDocTitles(xml, docId).get(0)));

        if (exploreThisTopicContent.isEmpty())
            logger.error(QUALIFICATION_INVALID + RECOMMENDED_TOPICS + "comment='no Explore this topic entries found in ATS xml.'" + target);
        else
            logger.info(QUALIFICATION_VALID + RECOMMENDED_TOPICS + "comment= Explore This topic entries collected from ATS xml.'" + target + " content=' " + exploreThisTopicContent + "'");

        List<String> alsoSee = extractor.getRecommendedTopicsAlsoSee(xml, docId);
        List<String> alsoSeeText = extractor.getRecommendedTopicsAlsoSeeText(xml, docId);
        List<String> atsContentReferences = extractor.getAtsContentReferences(ats);

        if (!(alsoSee.isEmpty() && alsoSeeText.isEmpty())) {

            alsoSee.forEach(search -> {
                if (search.matches("^[a-zA-Z0-9]*$"))
                    logger.info(QUALIFICATION_VALID + RECOMMENDED_TOPICS + "comment='Also See section search term does not contain invalid characters.'" + target + " content=' " + search + "'");
                else
                    logger.error(QUALIFICATION_INVALID + RECOMMENDED_TOPICS + "comment='Also See section search term does contain invalid characters, punctuations, or spaces.'" + target + " content=' " + search + "'");

                if (helper.doesContain(atsContentReferences, search))
                    logger.info(QUALIFICATION_VALID + RECOMMENDED_TOPICS + "comment='Also See section search term is properly located in ATS xml content reference URI.'" + target);
                else
                    logger.error(QUALIFICATION_INVALID + RECOMMENDED_TOPICS + "comment='Also See section search term is missing from ATS xml content reference URI.'" + target + " content=' " + search + "'");
            });

            alsoSeeText.forEach(entry -> {
                if (entry.startsWith("Enter"))
                    logger.error(QUALIFICATION_INVALID + RECOMMENDED_TOPICS + "comment='Also See section is not filled properly (starts with the word: Enter)!'" + target);

                if (helper.doesContain(exploreThisTopicContent, entry))
                    logger.error(QUALIFICATION_INVALID + RECOMMENDED_TOPICS + "comment='Also See recommended topic widget section entry overlaps with Explore This Topic entries!'" + target + " content=' " + entry + "'");
                else
                    logger.info(QUALIFICATION_INVALID + RECOMMENDED_TOPICS + "comment='Also See recommended topic widget section entry does not overlap with Explore This Topic entries.'" + target + " content=' " + entry + "'");
            });
        }
    }

    public void validateKeyPrimarySource(Document xml, String docId) {

        if (extractor.hasKeyPrimary(xml, docId)) {

            List<String> invalidDocs = extractor.getKeyPrimaryDocsWithTextOnly(xml, docId);

            if (!invalidDocs.isEmpty())
                logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Key Primary Sources section contains invalid document that has only text content.'" + target);

            List<String> keyPrimaryDocs = extractor.getKeyPrimaryDocs(xml, docId);
            List<String> keyPrimaryDocTypes = extractor.getKeyPrimaryDocTypes(xml, docId);

            if (keyPrimaryDocTypes.size() != keyPrimaryDocs.size()) {
                if (keyPrimaryDocTypes.size() < keyPrimaryDocs.size())
                    logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Key Primary Sources section is missing document type fields.'" + target);
                else
                    logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Key Primary Sources section contains more document type fields than documents.'" + target);
            } else
                logger.info(QUALIFICATION_VALID + KEY_PRIMARY_SOURCE + "comment='Key Primary Sources section contains document type fields in adequate numbers.'" + target);

            List<String> keyPrimaryOrders = extractor.getKeyPrimaryOrder(xml, docId);
            List<String> refinedSearchValues = new ArrayList<>();
            final int[] count = {0};

            if (!(keyPrimaryOrders.isEmpty() || keyPrimaryDocTypes.isEmpty())) {

                for (int i = 0; i < keyPrimaryOrders.size(); i++) {
                    List<String> linkTitle = extractor.getKeyPrimaryLinksTitle(xml, docId, helper.getElementByindex(keyPrimaryOrders, i));

                    if (helper.getElementByindex(keyPrimaryDocTypes, i).equals("Regulations") || helper.getElementByindex(keyPrimaryDocTypes, i).equals("Temporary Regulations")
                            || helper.getElementByindex(keyPrimaryDocTypes, i).equals("Proposed Regulations")) {

                        List<String> regType = extractor.getKeyPrimaryRegTypes(xml, docId, helper.getElementByindex(keyPrimaryOrders, i));

                        if (!regType.isEmpty()) {
                            switch (helper.getElementByindex(keyPrimaryDocTypes, i)) {
                                case ("Regulations"):
                                    if (helper.getElementByindex(regType, 0).contains("PROPOSED"))
                                        logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Document-type metadata is invalid for reg-type.'" + target + " content='order: "
                                                + helper.getElementByindex(keyPrimaryOrders, i) + " " + helper.getElementByindex(linkTitle, 0) + "'");
                                    else if (helper.getElementByindex(regType, 0).equals("FINAL"))
                                        logger.info(QUALIFICATION_VALID + KEY_PRIMARY_SOURCE + "comment='reg-type metadata is filled properly for Regulations document.'" + target);
                                    else
                                        logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='reg-type metadata is not filled properly for Regulations document!" + target
                                                + " content='order:" + helper.getElementByindex(keyPrimaryOrders, i) + " " + helper.getElementByindex(linkTitle, 0) + "'");
                                    break;

                                case ("Temporary Regulations"):
                                    if (helper.getElementByindex(regType, 0).contains("PROPOSED"))
                                        logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Document-type metadata is invalid for reg-type.'" + target + " content='order: "
                                                + helper.getElementByindex(keyPrimaryOrders, i) + " " + helper.getElementByindex(linkTitle, 0) + "'");
                                    else if (helper.getElementByindex(regType, 0).equals("TEMPORARY"))
                                        logger.info(QUALIFICATION_VALID + KEY_PRIMARY_SOURCE + "comment='reg-type metadata is filled properly for Temporary Regulations document.'" + target);
                                    else
                                        logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='reg-type metadata is not filled properly for Temporary Regulations document!" + target
                                                + " content='order:" + helper.getElementByindex(keyPrimaryOrders, i) + " " + helper.getElementByindex(linkTitle, 0) + "'");
                                    break;

                                case ("Proposed Regulations"):
                                    if (helper.getElementByindex(regType, 0).equals("PROPOSED REGULATION"))
                                        logger.info(QUALIFICATION_VALID + KEY_PRIMARY_SOURCE + "comment=' reg-type metadata is filled properly for Proposed Regulations document.'" + target);
                                    else
                                        logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='reg-type metadata is not filled properly for Proposed Regulations document!" + target
                                                + " content='order: " + helper.getElementByindex(keyPrimaryOrders, i) + " " + helper.getElementByindex(linkTitle, 0) + "'");
                                    break;

                                default:
                                    logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Invalid document type." + target + " content=' " + helper.getElementByindex(linkTitle, 0) + "'");
                                    break;
                            }
                        } else
                            logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='reg-type metadata field is missing from xml.'" + target + " content= order: " + helper.getElementByindex(keyPrimaryOrders, i)
                                    + " " + helper.getElementByindex(linkTitle, 0) + "'");
                    }
                    if (helper.getElementByindex(keyPrimaryOrders, i).contains("Regulations") || helper.getElementByindex(keyPrimaryOrders, i).equals("Laws") || helper.getElementByindex(keyPrimaryOrders, i).equals("IRC")) {
                        List<String> citations = extractor.getKeyPrimaryCitations(xml, docId, helper.getElementByindex(keyPrimaryOrders, i));

                        if (!citations.isEmpty()) {
                            if (citations.size() > 1)
                                logger.warn(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Laws/Regulations/IRC type document contains more than one citation metadata.'" + target
                                        + " content='order: " + helper.getElementByindex(keyPrimaryOrders, i) + " " + helper.getElementByindex(linkTitle, 0) + "'");

                            logger.info(QUALIFICATION_VALID + KEY_PRIMARY_SOURCE + "comment='Laws/Regulations/IRC type document contains citation metadata.'" + target);

                            if (helper.getElementByindex(keyPrimaryDocTypes, i).equals("Temporary Regulations")) {
                                citations.forEach(citation -> {
                                    if (!(citation.startsWith("Temporary Reg.") || citation.startsWith("Temp. Reg.") || citation.endsWith("T")))
                                        logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Temporary Regulations type document citation is not valid.'" + target + " content='" + citation + "'");
                                });

                            }
                        } else
                            logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Laws/Regulations/IRC type document is missing citation metadata.'" + target + " content='order: "
                                    + helper.getElementByindex(keyPrimaryOrders, i) + " " + helper.getElementByindex(linkTitle, 0) + "'");
                    }

                    List<String> keyPrimarySearchValues = extractor.getKeyPrimaryLinksSearchValues(xml, docId, helper.getElementByindex(keyPrimaryOrders, i));

                    if (helper.doesContain(keyPrimarySearchValues, "SEnterXREFhere."))
                        logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Key Primary link search value contains placeholder text (SEnterXREFhere.).'" + target + " content='order: "
                                + helper.getElementByindex(keyPrimaryOrders, i));

                    if (!keyPrimarySearchValues.isEmpty()) {
                        logger.info(QUALIFICATION_VALID + KEY_PRIMARY_SOURCE + "comment='Key Primary Section document contains the necessary link.'" + target);

                        String value = helper.getElementByindex(keyPrimarySearchValues, 0);

                        if (value.equals("S"))
                            logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Key Primary Section document link search value is only S.'" + target);

                        else if (value.contains("-") && value.contains(")") && value.indexOf("-") < value.indexOf("("))
                            refinedSearchValues.add(value.substring(value.indexOf("S") + 1, value.indexOf("(")));

                        else if (value.contains("-") && value.contains(")") && value.indexOf("-") > value.indexOf(")") && value.lastIndexOf(")") != value.indexOf(")"))
                            refinedSearchValues.add(value.substring(value.indexOf("S") + 1, value.lastIndexOf("-") + 2));

                        else if (value.contains("-") && value.contains(")") && value.indexOf("-") > value.indexOf(")"))
                            refinedSearchValues.add(value.substring(value.indexOf("S") + 1, value.lastIndexOf("-") + 2));

                        else if (!(value.contains(".") && value.contains(")") && value.contains("-")) && value.startsWith("S"))
                            refinedSearchValues.add(value.substring(value.indexOf("S") + 1));

                        else refinedSearchValues.add(value);

                        if (keyPrimarySearchValues.size() > 1)
                            logger.warn(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Key Primary document contains more than one associated link.'" + target + " content='order: "
                                    + keyPrimaryOrders.get(i));
                    } else
                        logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Key Primary document is missing the necessary link.'" + target + " content='order: "
                                + helper.getElementByindex(keyPrimaryOrders, i));
                }
                if (!refinedSearchValues.isEmpty()) {
                    refinedSearchValues.stream().distinct().forEach(refValue -> {
                        count[0] = Collections.frequency(refinedSearchValues, refValue);

                        if (count[0] > 1)
                            logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='document link search values overlap!'" + target + " content='" + refValue + " count:" + count[0] + "'");

                    });
                }
            } else
                logger.error(QUALIFICATION_INVALID + KEY_PRIMARY_SOURCE + "comment='Key Primary Section documents numbering orders are missing from xml.'" + target);
        }
    }

    public void validateFormsAndInstructions(Document xml, String docId) {

        if (extractor.hasFormsSection(xml, docId)) {
            List<String> formOrders = extractor.getFormsOrders(xml, docId);
            Map<String, List<String>> formNumberTypesMap = new HashMap<>();

            if (!formOrders.isEmpty()) {
                for (int i = 0; i < formOrders.size(); i++) {
                    List<String> docTypes = extractor.getFormsAndInstructionsDocType(xml, docId, helper.getElementByindex(formOrders, i));
                    List<String> formNumbers = extractor.getFormsAndInstructionsFormNumber(xml, docId, helper.getElementByindex(formOrders, i));
                    List<String> filteredTypes = new ArrayList<>();

                    if (!docTypes.isEmpty()) {
                        logger.info(QUALIFICATION_VALID + FORMS_AND_INSTRUCTIONS + "comment='Forms & Instruction section document contains document type.'" + target + "content='order: " + helper.getElementByindex(formOrders, i)
                                + " " + helper.getElementByindex(docTypes, 0) + "'");

                        filteredTypes.add(helper.getElementByindex(docTypes, 0));

                        if (docTypes.size() > 1)
                            logger.warn(QUALIFICATION_INVALID + FORMS_AND_INSTRUCTIONS + "comment='Forms & Instruction section document has more than one type.'" + target + "content='order: " + helper.getElementByindex(formOrders, i)
                                    + " " + helper.getElementByindex(formNumbers, 0) + "'");
                    } else
                        logger.error(QUALIFICATION_INVALID + FORMS_AND_INSTRUCTIONS + "comment='Forms & Instruction section document is missing document type.'" + target + "content='order: " + helper.getElementByindex(formOrders, i)
                                + " " + helper.getElementByindex(formNumbers, 0) + "'");

                    if (!formNumbers.isEmpty()) {
                        logger.info(QUALIFICATION_VALID + FORMS_AND_INSTRUCTIONS + "comment='Forms & Instructions section document contains form number." + target + "content='order: " + helper.getElementByindex(formOrders, i)
                                + " number: " + helper.getElementByindex(formNumbers, 0));

                        if (!docTypes.isEmpty()) {

                            if (!formNumberTypesMap.containsKey(formNumbers.get(0)))
                                formNumberTypesMap.put(helper.getElementByindex(formNumbers, 0), filteredTypes);
                            else {
                                List<String> values = formNumberTypesMap.get(helper.getElementByindex(formNumbers, 0));

                                values.add(helper.getElementByindex(docTypes, 0));
                                formNumberTypesMap.put(helper.getElementByindex(formNumbers, 0), values);
                            }

                            if (formNumbers.size() > docTypes.size())
                                logger.warn(QUALIFICATION_INVALID + FORMS_AND_INSTRUCTIONS + "comment='Forms & Instructions section document contains more than one form number.'" + target + "content='order: " + formOrders.get(i)
                                        + " " + helper.getElementByindex(formNumbers, 0) + "'");
                        }
                    } else
                        logger.error(QUALIFICATION_INVALID + FORMS_AND_INSTRUCTIONS + "comment='Forms & Instructions section document is missing document number.'" + target + "content='order: " + formOrders.get(i)
                                + " " + helper.getElementByindex(formNumbers, 0) + "'");
                }
                final int[] count = {0};
                if (!formNumberTypesMap.isEmpty()) {
                    formNumberTypesMap.keySet().forEach(formNumber -> {
                        List<String> docTypeListFromMap = formNumberTypesMap.get(formNumber);

                        docTypeListFromMap.forEach(type -> {
                            count[0] = helper.countInstances(docTypeListFromMap, type);
                        });
                        if (count[0] > 1)
                            logger.error(QUALIFICATION_INVALID + FORMS_AND_INSTRUCTIONS + "comment='Forms & Instructions section document types are not unique!'" + target);
                    });
                }
            } else
                logger.error(QUALIFICATION_INVALID + FORMS_AND_INSTRUCTIONS + "comment='document with Forms & Instructions section is missing section content.'" + target);
        }
    }
}

