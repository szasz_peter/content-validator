package com.epam.idcollector;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Student on 7/31/2017.
 */
public class Helper {

    public static Document parseXML(String path) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory myDocBuilder = DocumentBuilderFactory.newInstance();
        myDocBuilder.setValidating(false);
        try {
            myDocBuilder.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        DocumentBuilder dBuilder = myDocBuilder.newDocumentBuilder();

        Document doc = dBuilder.parse(path);
        doc.getDocumentElement().normalize();

        return doc;
    }

    public static <T extends Node> List<T> iterable(NodeList list, Class<T> klass) {
        final List<T> nodes = new ArrayList<>(list.getLength());
        for (int i = 0; i < list.getLength(); ++i) {
            nodes.add(klass.cast(list.item(i)));
        }
        return nodes;
    }

    public static boolean doesContain(List<String> list, String listElement) {
        return list.contains(listElement);
    }

    public static String normalizeString(String inputString) {
        return inputString.replace("\r\n", " ").replace("\n", " ").replace("\t", "").replace("_", " ").trim();
    }

    public int countInstances(List<String> toAnalyze, String search) {
        final int[] count = {0};

        toAnalyze.forEach(listElement -> {
            if (listElement.equals(search)) count[0]++;
        });

        return count[0];
    }

    public String getElementByindex(List<String> extractedList, int index) {
        if (extractedList.isEmpty())
            return null;
        return extractedList.get(index);
    }

}
