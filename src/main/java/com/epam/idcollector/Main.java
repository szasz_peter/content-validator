
package com.epam.idcollector;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.epam.idcollector.Extractor.Extractor;
import org.apache.log4j.PropertyConfigurator;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

import static com.epam.idcollector.Helper.parseXML;

/**
 * @author Student
 */
public class Main {

    public static Map<String, List<String>> imageMap = new HashMap<>();
    public static String atlasTopicSetPath;
    private static List<String> ipackPaths = new ArrayList<>();
    private static Validator validator = new Validator();
    private static List<String> images = new ArrayList<>();
    private static Extractor extractor = new Extractor();
    private static final String FILE_SEPARATOR = "\\";
    public static String target = "";

    static {
        PropertyConfigurator.configure("src/main/resources/log4j.properties");
    }

    public static void main(String[] args) {
        String path = "src/main/resources/TLP";

        fileCollector(path);

        if (ipackPaths.isEmpty()) throw new RuntimeException("No IPACKs are found!");
        if (atlasTopicSetPath.isEmpty()) throw new RuntimeException("No ATS xml is found!");

        Document ats = null;
        try {
            ats = parseXML(atlasTopicSetPath);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        List<String> atlasTopicNodeIds = extractor.getAtlasTopicIds(ats);
        List<String> allMetaDataAtsNodeIds = new ArrayList<>();

        atlasTopicNodeIds.remove("AC_TLP_C_Corporations");
        atlasTopicNodeIds.remove("AC_TLP_Estates_Gifts_and_Trusts");
        atlasTopicNodeIds.remove("AC_TLP_Special_Entities");

        ipackPaths.parallelStream().forEach(ipackPath -> {
            try {
                Document xml = parseXML(ipackPath);

                List<String> iDs = extractor.getDocumentId(xml);

                iDs.forEach(docId -> {

                    target = " target ='" + docId + " (" + extractor.getDocTitles(xml, docId).get(0) + " - " +
                            ipackPath.substring(ipackPath.lastIndexOf(FILE_SEPARATOR) + FILE_SEPARATOR.length(), ipackPath.lastIndexOf("_WK-US")) + ")'";

                    validator.validateContributors(xml, docId);
                    validator.validateOverview(xml, docId);
                    if (ipackPath.substring(ipackPath.lastIndexOf(FILE_SEPARATOR) + FILE_SEPARATOR.length()).equals("tpj01_WK-US_DOCUMENTS.xml")) {
                        validator.validateStateWidgets(xml, docId);
                    }
                    validator.validateRecommendedTopics(xml, docId);
                    validator.validateKeyPrimarySource(xml, docId);
                    validator.validateFormsAndInstructions(xml, docId);

                    List<String> metaDataATSNodeIds = extractor.getMetaDataAtsNodeIds(xml, docId);
                    validator.validateMetaDataAtsNodes(metaDataATSNodeIds, atlasTopicNodeIds);
                    allMetaDataAtsNodeIds.addAll(metaDataATSNodeIds);
                });
            } catch (IOException | SAXException | ParserConfigurationException e) {
                e.printStackTrace();
            }
        });
        validator.validateAts(allMetaDataAtsNodeIds, atlasTopicNodeIds);
    }

    private static void fileCollector(String path) {

        File f = new File(path);

        File[] files = f.listFiles();

        for (File file : files) {

            if (file.isDirectory() && file.list().length != 0) {

                fileCollector(file.getPath());
            } else if (file.isFile()) {

                if (file.getName().endsWith(".png")) {
                    images.add(file.getName());
                    imageMap.put(file.getParentFile().getParentFile().getName(), images);
                }

                if ("tpj01_WK-US_DOCUMENTS.xml".equals(file.getName()) || "tpy01_WK-US_DOCUMENTS.xml".equals(file.getName())) {
                    ipackPaths.add(file.getAbsolutePath());
                }
                if ("wk-taa-ac-tlp-topics-ats.xml".equals(file.getName())) atlasTopicSetPath = file.getAbsolutePath();
            }
        }
    }

}
