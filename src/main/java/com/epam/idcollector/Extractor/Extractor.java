package com.epam.idcollector.Extractor;

import com.epam.idcollector.Helper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.epam.idcollector.Helper.iterable;
import static com.epam.idcollector.Helper.normalizeString;


/**
 * Created by Student on 8/1/2017.
 */
public class Extractor {
    private static Helper helper = new Helper();
    private static final String FILE_SEPARATOR = "\\";

    public List<String> getDocumentId(Document xml) {
        return iterable(xml.getElementsByTagName("wkdoc:document"), Element.class).stream()
                .map(element -> element.getAttribute("id"))
                .collect(Collectors.toList());
    }

    public List<String> getContributorName(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("contributor-name"), Element.class).stream())
                .map(element -> element.getTextContent())
                .collect(Collectors.toList());
    }

    public List<String> getContributorAffiliation(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("affiliation"), Element.class).stream())
                .map(element -> element.getTextContent())
                .collect(Collectors.toList());
    }

    public List<String> getContributorBiography(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("biography"), Element.class).stream())
                .map(element -> element.getTextContent())
                .collect(Collectors.toList());
    }

    public List<String> getContributorImages(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("graphic"), Element.class).stream())
                .map(element -> element.getAttribute("src"))
                .collect(Collectors.toList());
    }

    public List<String> getVideoLinks(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("analytical-commentary"))
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("wklink:url"), Element.class).stream())
                        .filter(element2 -> !element2.getTextContent().contains("Interactive Infographic")))
                .map(element1 -> element1.getAttribute("ref-url"))
                .collect(Collectors.toList());
    }

    public List<String> getInlineInfographicImages(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("analytical-commentary"))
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("inline-image"), Element.class).stream()))
                .map(element1 -> element1.getAttribute("src"))
                .collect(Collectors.toList());
    }

    public List<String> getInteractiveInfoGraphics(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("analytical-commentary"))
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("wklink:url"), Element.class).stream())
                        .filter(element2 -> element2.getTextContent().contains("Interactive Infographic")))
                .map(element1 -> element1.getAttribute("ref-url"))
                .collect(Collectors.toList());
    }

    public List<String> getNestedWkdocs(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("analytical-commentary"))
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("wkdoc:level"), Element.class).stream())
                        .filter(element2 -> !(element2.getAttribute("css-class").equals("wk-document-media")
                                || element2.getAttribute("css-class").equals("wk-document-media wk-thumbnail-medium wk-float-right")
                                || element2.getAttribute("css-class").equals("wk-document-media wk-thumbnail-small wk-float-right")
                                || element2.getAttribute("css-class").equals("wk-document-media wk-thumbnail-medium")
                                || element2.getAttribute("css-class").equals("wk-document-media wk-thumbnail-small")
                                || element2.getAttribute("css-class").equals("wk-document-media wk-thumbnail-small wk-float-left")
                                || element2.getAttribute("css-class").equals("multi-media"))))
                .map(element1 -> element1.getAttribute("css-class"))
                .collect(Collectors.toList());
    }

    public List<String> getLinksTools(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("analytical-commentary"))
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("span"), Element.class).stream())
                        .filter(element1 -> element1.getAttribute("css-class").equals("ats-node-ref")))
                .map(element1 -> element1.getAttribute("css-class"))
                .collect(Collectors.toList());
    }

    public List<String> getLinksToolsTitles(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("analytical-commentary"))
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("span"), Element.class).stream())
                        .filter(element1 -> element1.getAttribute("property").equals("title")))
                .map(element1 -> element1.getAttribute("property"))
                .collect(Collectors.toList());
    }

    public List<String> getLinksToolsVisibleText(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("analytical-commentary"))
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("span"), Element.class).stream())
                        .filter(element1 -> element1.getAttribute("property").equals("visible-text")))
                .map(element1 -> element1.getAttribute("property"))
                .collect(Collectors.toList());
    }

    public List<String> getLinksToolsNodeRef(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("analytical-commentary"))
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("span"), Element.class).stream())
                        .filter(element1 -> element1.getAttribute("property").equals("ats-node-ref")))
                .map(element1 -> element1.getAttribute("property"))
                .collect(Collectors.toList());
    }

    public List<String> getAtlasTopicIds(Document xml) {
        return iterable(xml.getElementsByTagName("AtlasTopic"), Element.class).stream()
                .filter(element -> element.getAttribute("Id").equals("AC_TLP_Federal") || element.getAttribute("Id").equals("AC_TLP_State"))
                .flatMap(element -> iterable(element.getElementsByTagName("AtlasTopic"), Element.class).stream())
                .map(element1 -> element1.getAttribute("Id"))
                .collect(Collectors.toList());
    }

    public List<String> getMetaDataAtsNodeIds(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkext-meta:attr"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("name").equals("ats-node-id")))
                .map(element1 -> element1.getAttribute("value"))
                .collect(Collectors.toList());
    }

    public List<String> getStateWidgets(Document xml, String docId) {
        boolean isState = filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("heading"), Element.class).stream())
                .anyMatch(element -> element.getTextContent().equals("Treatment by State"));

        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("state-multi-juisdictional-grid") && isState))
                .flatMap(element1 -> iterable(element1.getElementsByTagName("span"), Element.class).stream())
                .filter(element2 -> element2.getAttribute("property").equals("taxtype"))
                .map(element2 -> helper.normalizeString(element2.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getStateWidgetTaxTypes(Document xml, String docId) {
        boolean isState = filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("heading"), Element.class).stream())
                .anyMatch(element -> element.getTextContent().equals("Treatment by State"));

        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("state-multi-juisdictional-grid") && isState)
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("wklink:cite-ref"), Element.class).stream()))
                .map(element1 -> helper.normalizeString(element1.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getRecommendedTopicsExploreTopics(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> getChildElementsByTagName(element, "wkdoc:level"))
                .flatMap(element -> getChildElementsByTagName(element, "wkdoc:level"))
                .filter(element -> getChildElementsByTagName(element, "heading").anyMatch(element1 -> normalizeString(element1.getTextContent()).equals("Explore this Topic")))
                .map(element -> normalizeString(element.getTextContent()).trim())
                .collect(Collectors.toList());
    }

    public List<String> getContentOfExploreTopics(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> getChildElementsByTagName(element, "wkdoc:level"))
                .filter(element -> element.getAttribute("css-class").equals("multi-document-list"))
                .flatMap(element -> getChildElementsByTagName(element, "wkdoc:level"))
                .filter(element -> getChildElementsByTagName(element, "heading").anyMatch(element1 -> normalizeString(element1.getTextContent()).equals("Explore this Topic")))
                .flatMap(element -> getChildElementsByTagName(element, "para"))
                .flatMap(element1 -> iterable(element1.getElementsByTagName("wklink:cite-ref"), Element.class).stream())
                .map(element -> normalizeString(element.getTextContent()).trim())
                .collect(Collectors.toList());
    }

    public List<String> getExploreThisTopicFromAts(Document ats, String docTitle) {
        return iterable(ats.getElementsByTagName("AtlasTopic"), Element.class).stream()
                .filter(element -> getChildElementsByTagName(element, "Name").anyMatch(element1 -> normalizeString(element1.getTextContent()).equals(docTitle)))
                .flatMap(element -> getChildElementsByTagName(element, "AtlasTopic"))
                .map(element -> normalizeString(element.getAttribute("Id")).replace("AC TLP", "").trim())
                .collect(Collectors.toList());
    }

    public List<String> getExploreThisTopicParent(Document ats, String docTitle) {
        return iterable(ats.getElementsByTagName("AtlasTopic"), Element.class).stream()
                .filter(element -> getChildElementsByTagName(element, "Name").anyMatch(element1 -> normalizeString(element1.getTextContent()).equals(docTitle)))
                .map(element -> normalizeString(element.getAttribute("Id")).replace("AC TLP", "").trim())
                .collect(Collectors.toList());
    }

    public List<String> getRecommendedTopicsAlsoSee(Document xml, String docId) {
        return getRecommendedTopicsAlso(xml, docId)
                .map(element1 -> element1.getAttribute("search-value").toLowerCase())
                .collect(Collectors.toList());
    }

    public List<String> getRecommendedTopicsAlsoSeeText(Document xml, String docId) {
        return getRecommendedTopicsAlso(xml, docId)
                .map(element1 -> element1.getTextContent())
                .collect(Collectors.toList());
    }

    public List<String> getAtsContentReferences(Document ats) {
        return iterable(ats.getElementsByTagName("ContentReference"), Element.class).stream()
                .filter(element -> element.hasAttributes() && !element.getAttribute("URIvalue").isEmpty())
                .map(element -> helper.normalizeString(element.getAttribute("URIvalue").substring(element.getAttribute("URIvalue").lastIndexOf("/") + FILE_SEPARATOR.length())).trim().toLowerCase())
                .collect(Collectors.toList());
    }

    public List<String> getKeyPrimaryDocs(Document xml, String docId) {
        return filterForKeyPrimary(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("para"), Element.class).stream())
                .filter(element -> element.getChildNodes().getLength() != 1)
                .map(element -> normalizeString(element.getAttribute("align")))
                .collect(Collectors.toList());
    }

    public List<String> getKeyPrimaryDocsWithTextOnly(Document xml, String docId) {
        return filterForKeyPrimary(xml, docId)
                .flatMap(element -> getChildElementsByTagName(element, "para"))
                .filter(element -> element.getChildNodes().getLength() == 1 && !element.getFirstChild().getTextContent().isEmpty())
                .map(element -> normalizeString(element.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getKeyPrimaryDocTypes(Document xml, String docId) {
        return filterForKeyPrimary(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("para"), Element.class).stream())
                .filter(element -> element.getChildNodes().getLength() != 1)
                .flatMap(element -> iterable(element.getElementsByTagName("span"), Element.class).stream())
                .filter(element -> element.getAttribute("property").equals("document-type"))
                .map(element -> normalizeString(element.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getKeyPrimaryOrder(Document xml, String docId) {
        return filterForKeyPrimary(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("para"), Element.class).stream())
                .filter(element -> element.getChildNodes().getLength() != 1)
                .flatMap(element -> iterable(element.getElementsByTagName("span"), Element.class).stream())
                .filter(element -> element.getAttribute("property").equals("order"))
                .map(element -> normalizeString(element.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getKeyPrimaryRegTypes(Document xml, String docId, String orderNumber) {
        return filterKeyPrimaryMetaDatas(xml, docId, orderNumber)
                .flatMap(element -> getChildElementsByTagName(element, "span"))
                .filter(element -> element.getAttribute("property").equals("reg-type"))
                .map(element -> normalizeString(element.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getKeyPrimaryCitations(Document xml, String docId, String orderNumber) {
        return filterKeyPrimaryMetaDatas(xml, docId, orderNumber)
                .flatMap(element -> getChildElementsByTagName(element, "span"))
                .filter(element -> element.getAttribute("property").equals("citation"))
                .map(element -> normalizeString(element.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getKeyPrimaryLinksSearchValues(Document xml, String docId, String orderNumber) {
        return filterKeyPrimaryMetaDatas(xml, docId, orderNumber)
                .flatMap(element -> getChildElementsByTagName(element, "wklink:cite-ref"))
                .map(element -> element.getAttribute("search-value"))
                .collect(Collectors.toList());
    }

    public List<String> getKeyPrimaryLinksTitle(Document xml, String docId, String orderNumber) {
        return filterKeyPrimaryMetaDatas(xml, docId, orderNumber)
                .flatMap(element -> getChildElementsByTagName(element, "wklink:cite-ref"))
                .map(element -> normalizeString(element.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getFormsOrders(Document xml, String docId) {
        return filterForFormsSection(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("para"), Element.class).stream())
                .filter(element -> element.getChildNodes().getLength() != 1)
                .flatMap(element -> iterable(element.getElementsByTagName("span"), Element.class).stream())
                .filter(element -> element.getAttribute("property").equals("order"))
                .map(element -> normalizeString(element.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getFormsAndInstructionsDocType(Document xml, String docId, String orderNumber) {
        return filterFormsDocsByOrder(xml, docId, orderNumber)
                .flatMap(element -> iterable(element.getElementsByTagName("span"), Element.class).stream())
                .filter(element -> element.getAttribute("property").equals("document-type"))
                .map(element -> normalizeString(element.getTextContent()))
                .collect(Collectors.toList());
    }

    public List<String> getFormsAndInstructionsFormNumber(Document xml, String docId, String orderNumber) {
        return filterFormsDocsByOrder(xml, docId, orderNumber)
                .flatMap(element -> iterable(element.getElementsByTagName("span"), Element.class).stream())
                .filter(element -> element.getAttribute("property").equals("form-number"))
                .map(element -> normalizeString(element.getTextContent()))
                .collect(Collectors.toList());
    }

    private Stream<Element> filterFormsDocsByOrder(Document xml, String docId, String orderNumber) {
        return filterForFormsSection(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("para"), Element.class).stream())
                .filter(element -> element.getChildNodes().getLength() != 1 && getChildElementsByTagName(element, "span")
                        .anyMatch(element1 -> element1.getAttribute("property").equals("order") && element1.getTextContent().equals(orderNumber)));
    }

    public boolean hasFormsSection(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("heading"), Element.class).stream())
                .anyMatch(element -> element.getTextContent().equals("Forms") || element.getTextContent().equals("Forms & Instructions") || element.getTextContent().equals("Forms &amp; Instructions"));
    }

    private Stream<Element> filterForFormsSection(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> getChildElementsByTagName(element, "wkdoc:level"))
                .filter(element -> element.getAttribute("css-class").equals("doc-tiles-with-sublinks") && hasFormsSection(xml, docId));
    }

    private Stream<Element> filterKeyPrimaryMetaDatas(Document xml, String docId, String orderNumber) {
        return filterForKeyPrimary(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("para"), Element.class).stream())
                .filter(element -> element.getChildNodes().getLength() != 1 && getChildElementsByTagName(element, "span")
                        .anyMatch(element1 -> element1.getAttribute("property").equals("order") && element1.getTextContent().equals(orderNumber)));
    }

    public boolean hasKeyPrimary(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("heading"), Element.class).stream())
                .anyMatch(element -> element.getTextContent().equals("Key Primary Sources"));
    }

    private Stream<Element> filterForKeyPrimary(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> getChildElementsByTagName(element, "wkdoc:level"))
                .filter(element -> element.getAttribute("css-class").equals("linkable-doc-tiles-with-tabs") && hasKeyPrimary(xml, docId));
    }

    private static Stream<Element> getChildElementsByTagName(Element element, String tagName) {
        return iterable(element.getChildNodes(), Node.class).stream()
                .filter(n -> n.getNodeType() == Node.ELEMENT_NODE)
                .map(n -> (Element) n)
                .filter(e -> e.getTagName().equals(tagName));
    }

    public List<String> getDocTitles(Document xml, String docId) {
        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("title"), Element.class).stream())
                .map(element -> element.getTextContent())
                .collect(Collectors.toList());

    }

    private Stream<Element> filterByDocID(Document xml, String docId) {
        return iterable(xml.getElementsByTagName("wkdoc:document"), Element.class).stream()
                .filter(element -> element.getAttribute("id").equals(docId));
    }

    private Stream<Element> getRecommendedTopicsAlso(Document xml, String docId) {
        boolean isRecommendedTopic = filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("heading"), Element.class).stream())
                .anyMatch(element -> element.getTextContent().equals("Recommended Topics"));

        return filterByDocID(xml, docId)
                .flatMap(element -> iterable(element.getElementsByTagName("wkdoc:level"), Element.class).stream()
                        .filter(element1 -> element1.getAttribute("css-class").equals("multi-document-list") && isRecommendedTopic)
                        .flatMap(element1 -> iterable(element1.getElementsByTagName("wklink:cite-ref"), Element.class).stream()));
    }

}
