<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TM2ATSControl
          PUBLIC "-//WoltersKluwer//DTD TM2ATS Control//EN"
          "tm2atscontrol.dtd">

<!-- *******************************************************************
  ** Topic set control file.
  ** *******************************************************************
  ** This file is intended for use with the various ".xtm" files
  ** that are extracted from the relationship database for conversion
  ** into Atlas Topic Sets.
  ** *******************************************************************
  ** Note that this control file must contain a
  ** "PSIDefinitions/TopLevelNodeUsage/ResourceData" element and a
  ** "ContentReferences/ContentReference" element definition for
  ** each different "*.xtm" with which it is to be used.
  ** *******************************************************************
  ** Resource Data              Content Reference
  ** =============              =================
  **
  ** Employment Practices       http://cch.com/xtm/epg/
  ** Fed All                    http://cch.com/xtm/fedCodeSec/
  ** Fed Regs                   http://cch.com/xtm/fedRegs/
  ** Fed Topical                http://cch.com/xtm/fedtax/
  ** Kare                       http://cch.com/xtm/kare/
  ** Key Phrase Relates         http://cch.com/xtm/statetax/
  ** Labor Relations            http://cch.com/xtm/lr/
  ** Pay                        http://cch.com/xtm/pay/
  ** Pen Relates                http://cch.com/xtm/pen/
  ** Securities                 http://cch.com/xtm/sec/
  ** Wages Hours                http://cch.com/xtm/wh/
  **
  ** Alaska                     http://cch.com/xtm/stak/
  ** Alabama                    http://cch.com/xtm/stal/
  ** Arkansas                   http://cch.com/xtm/star/
  ** Arizona                    http://cch.com/xtm/staz/
  ** California                 http://cch.com/xtm/stca/
  ** Colorado                   http://cch.com/xtm/stco/
  ** Connecticut                http://cch.com/xtm/stct/
  ** District of Columbia       http://cch.com/xtm/stdc/
  ** Delaware                   http://cch.com/xtm/stde/
  ** Florida                    http://cch.com/xtm/stfl/
  ** Georgia                    http://cch.com/xtm/stga/
  ** Hawaii                     http://cch.com/xtm/sthi/
  ** Iowa                       http://cch.com/xtm/stia/
  ** Idaho                      http://cch.com/xtm/stid/
  ** Illinois                   http://cch.com/xtm/stil/
  ** Indiana                    http://cch.com/xtm/stin/
  ** Kansas                     http://cch.com/xtm/stks/
  ** Kentucky                   http://cch.com/xtm/stky/
  ** Louisiana                  http://cch.com/xtm/stla/
  ** Massachusetts              http://cch.com/xtm/stma/
  ** Maryland                   http://cch.com/xtm/stmd/
  ** Maine                      http://cch.com/xtm/stme/
  ** Michigan                   http://cch.com/xtm/stmi/
  ** Minnesota                  http://cch.com/xtm/stmn/
  ** Missouri                   http://cch.com/xtm/stmo/
  ** Mississippi                http://cch.com/xtm/stms/
  ** Montana                    http://cch.com/xtm/stmt/
  ** North Carolina             http://cch.com/xtm/stnc/
  ** North Dakota               http://cch.com/xtm/stnd/
  ** Nebraska                   http://cch.com/xtm/stne/
  ** New Hampshire              http://cch.com/xtm/stnh/
  ** New Jersey                 http://cch.com/xtm/stnj/
  ** New Mexico                 http://cch.com/xtm/stnm/
  ** Nevada                     http://cch.com/xtm/stnv/
  ** New York                   http://cch.com/xtm/stny/
  ** Ohio                       http://cch.com/xtm/stoh/
  ** Oklahoma                   http://cch.com/xtm/stok/
  ** Oregon                     http://cch.com/xtm/stor/
  ** Pennsylvania               http://cch.com/xtm/stpa/
  ** Rhode Island               http://cch.com/xtm/stri/
  ** South Carolina             http://cch.com/xtm/stsc/
  ** South Dakota               http://cch.com/xtm/stsd/
  ** Tennessee                  http://cch.com/xtm/sttn/
  ** Texas                      http://cch.com/xtm/sttx/
  ** Utah                       http://cch.com/xtm/stut/
  ** Virginia                   http://cch.com/xtm/stva/
  ** Vermont                    http://cch.com/xtm/stvt/
  ** Washington                 http://cch.com/xtm/stwa/
  ** Wisconsin                  http://cch.com/xtm/stwi/
  ** West Virginia              http://cch.com/xtm/stwv/
  ** Wyoming                    http://cch.com/xtm/stwy/
  ** New York City              http://cch.com/xtm/styc/
  **
  ** *******************************************************************
  ** Modification History
  **
  ** $Log: relationship_db_xtm_control.xml,v $
  ** Revision 1.4  2010/04/14 16:36:18  jrugh
  ** Checked in described changes.
  **
  ** . Modified by reimplementing the change made for Revision 1.2
  **
  ** Revision 1.3  2010/04/14 15:33:23  jrugh
  ** Checked in described changes.
  **
  ** . Modified by reverting the change made for Revision 1.2
  **   and added "AggregateDocumentRelationships" attribute
  **   to <ResourceData> for "Pen Relates".
  **
  ** Revision 1.2  2010/04/14 14:54:46  jrugh
  ** Checked in described changes.
  **
  ** . Modified by changing ATS generated for XTM
  **   with "toplevelFlag" for "Key Phrase Relates"
  **   from "TopicalRelate" to "StructuralRelate".
  **
  ** Revision 1.1  2010/04/14 11:46:15  jrugh
  ** Created new baseline version.
  **
  ** . Created initial unchanged version in "Shared-Taxonomies"
  **   CVS project.
  **
  ** Revision 1.4  2010/01/27 21:52:53  jrugh
  ** Checked in described changes.
  **
  ** . For "City of New York"
  **   . Added "ResourceData" ("City of New York").
  **   . Added "ContentReference" ("styc").
  **
  ** Revision 1.3  2007/09/28 11:36:13  jrugh
  ** Checked in described changes.
  **
  ** . Added "Type" attribute to all remaining "ResourceData" elements.
  **   Mantis 10209.
  **
  ** Revision 1.2  2007/09/21 13:06:07  jrugh
  ** Checked in described changes.
  **
  ** . Added "Type" attribute to "ResourceData" element for
  **   "Fed All" and "Fed Topical".
  **   Mantis 10209.
  **
  ** Revision 1.1  2007/05/28 15:59:27  jrugh
  ** Checked in described changes.
  **
  ** 2007/05/18 - Jack Rugh, Retrieval Systems Corp.
  ** . Added <ParentChild PSI="http://wolterskluwer.com/psi/#parent-child-hierarchy"/>
  **   to support URIstem="http://cch.com/xtm/stin/".
  ** . Added <Parent PSI="http://wolterskluwer.com/psi/#parent"/>
  **   to support URIstem="http://cch.com/xtm/stin/".
  ** . Added <Child PSI="http://wolterskluwer.com/psi/#child"/>
  **   to support URIstem="http://cch.com/xtm/stin/".
  ** . Ideally, the following single set of values would be used
  **   for all topic maps that use this control file:
  **     <Sort PSI="http://wolterskluwer.com/psi/#sort"/>
  **     <ParentChild PSI="http://wolterskluwer.com/psi/#parent-child-hierarchy"/>
  **     <Parent PSI="http://wolterskluwer.com/psi/#parent"/>
  **     <Child PSI="http://wolterskluwer.com/psi/#child"/>
  **
  ** 2007/05/17 - Jack Rugh, Retrieval Systems Corp.
  ** . Added many new <ResourceData> and <ContentReference> elements
  **   based on an Excel spreadsheet provided by Kavitha
  **   Lingamoorthy.
  **
  ** 2007/04/20 - Jack Rugh, Retrieval Systems Corp.
  ** . Created initial version.
  ** *******************************************************************
-->

<TM2ATSControl>

  <PSIDefinitions>

    <TopLevelNodeUsage PSI="http://wolterskluwer.com/psi/#toplevelFlag">
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Employment Practices</ResourceData>
      <ResourceData Usage="TopLevel" Type="StructuralRelate">Fed All</ResourceData>
      <ResourceData Usage="TopLevel" Type="StructuralRelate">Fed Regs</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Fed Topical</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Kare</ResourceData>
      <ResourceData Usage="TopLevel" Type="StructuralRelate">Key Phrase Relates</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Labor Relations</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Pay</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate" AggregateDocumentRelationships="y">Pen Relates</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Securities</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Wages Hours</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Alaska</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Alabama</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Arkansas</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Arizona</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">California</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Colorado</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Connecticut</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">District of Columbia</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Delaware</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Florida</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Georgia</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Hawaii</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Iowa</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Idaho</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Illinois</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Indiana</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Kansas</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Kentucky</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Louisiana</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Massachusetts</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Maryland</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Maine</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Michigan</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Minnesota</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Missouri</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Mississippi</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Montana</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">North Carolina</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">North Dakota</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Nebraska</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">New Hampshire</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">New Jersey</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">New Mexico</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Nevada</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">New York</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Ohio</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Oklahoma</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Oregon</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Pennsylvania</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Rhode Island</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">South Carolina</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">South Dakota</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Tennessee</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Texas</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Utah</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Virginia</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Vermont</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Washington</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Wisconsin</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">West Virginia</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">Wyoming</ResourceData>
      <ResourceData Usage="TopLevel" Type="TopicalRelate">City of New York</ResourceData>
    </TopLevelNodeUsage>

    <Display PSI="http://wolterskluwer.com/psi/#none_unscoped_basename_is_used"/>
    <Sort PSI="http://wolterskluwer.com/psi/#sort"/>
    <ParentChild PSI="http://wolterskluwer.com/psi/#parent-child-hierarchy"/>
    <ParentChild PSI="#parent-child-hierarchy"/>
    <Parent PSI="http://wolterskluwer.com/psi/#parent"/>
    <Parent PSI="http://www.techquila.com/psi/hierarchy/#superordinate-role-type"/>
    <Child PSI="http://wolterskluwer.com/psi/#child"/>
    <Child PSI="http://www.techquila.com/psi/hierarchy/#subordinate-role-type"/>

  </PSIDefinitions>

  <ContentReferences>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/epg/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/fedCodeSec/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/fedRegs/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/fedtax/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/kare/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/statetax/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/lr/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/pay/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/pen/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/sec/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/wh/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stak/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stal/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/star/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/staz/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stca/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stco/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stct/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stdc/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stde/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stfl/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stga/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/sthi/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stia/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stid/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stil/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stin/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stks/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stky/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stla/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stma/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stmd/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stme/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stmi/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stmn/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stmo/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stms/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stmt/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stnc/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stnd/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stne/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stnh/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stnj/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stnm/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stnv/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stny/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stoh/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stok/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stor/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stpa/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stri/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stsc/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stsd/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/sttn/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/sttx/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stut/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stva/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stvt/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stwa/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stwi/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stwv/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/stwy/"/>
    <ContentReference ElementName="topic" URIattr="uri" URIstem="http://cch.com/xtm/styc/"/>
  </ContentReferences>

</TM2ATSControl>

